<?php

return [
    'xml_path_daily' => 'https://www.nbkr.kg/XML/daily.xml',
    'xml_path_weekly' => 'https://www.nbkr.kg/XML/weekly.xml',
];
