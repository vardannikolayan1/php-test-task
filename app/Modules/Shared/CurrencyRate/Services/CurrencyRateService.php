<?php

namespace App\Modules\Shared\CurrencyRate\Services;

use App\Models\CurrencyRateLog;
use App\Modules\Shared\CurrencyRate\Enums\CurrencyEnum;
use Exception;

class CurrencyRateService implements CurrencyRateServiceInterface
{
    public $isoCode;
    public $rateValue;

    /**
     * @param  CurrencyEnum  $currency
     * @return float
     * @throws Exception
     */
    public function getCurrentRate(CurrencyEnum $currency): float
    {
        $xmlDailyDataObject = file_get_contents(config('currency.xml_path_daily'));
        $xmlWeeklyDataObject = file_get_contents(config('currency.xml_path_weekly'));

        $dailyRatesData = simplexml_load_string($xmlDailyDataObject);
        $currencyDailyRates = $dailyRatesData->Currency;

        $weeklyRatesData = simplexml_load_string($xmlWeeklyDataObject);
        $currencyWeeklyRates = $weeklyRatesData->Currency;

        if ($currency->name === 'KGS') {
            return 1;
        }

        foreach ($currencyDailyRates as $rate) {
            $isoCode = (string) $rate['ISOCode'];
            if ($currency->name === $isoCode) {
                $jsonString = json_encode($rate->Value);
                $rateArray = json_decode($jsonString, true);
                $this->rateValue = $this->convertToFloat($rateArray[0]) * (float) $rate->Nominal;
                $this->isoCode = $isoCode;

                $this->syncToDb();

                return $this->rateValue * (float) $rate->Nominal;
            }
        }

        foreach ($currencyWeeklyRates as $rate) {
            $isoCode = (string) $rate['ISOCode'];
            if ($currency->name === $isoCode) {
                $jsonString = json_encode($rate->Value);
                $rateArray = json_decode($jsonString, true);
                $this->rateValue = $this->convertToFloat($rateArray[0]) * (float) $rate->Nominal;
                $this->isoCode = $isoCode;

                $this->syncToDb();

                return $this->rateValue * (float) $rate->Nominal;
            }
        }

        throw new Exception("Currency rate not found for {$currency->name}");
    }

    /**
     * @return void
     */
    public function syncToDb(): void
    {
        CurrencyRateLog::create([
            'iso_code' => $this->isoCode,
            'rate_value' => $this->rateValue,
        ]);
    }

    /**
     * @param $value
     * @return string
     */
    public function convertToFloat($value): string
    {
        $normalizedValue = str_replace(',', '.', $value);
        $floatValue = (float) $normalizedValue;
        return number_format($floatValue, 4, '.', '');
    }
}
